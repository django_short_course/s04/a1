from django.urls import path
from . import views

urlpatterns = [
    path('logout', views.logout_user, name = 'logout'),
    path('login', views.login_user, name ='login'),
    path('change_password', views.change_password, name = 'change_password' ),
    path('register', views.register, name = 'register'),
    path('<int:groceryitem_id>/', views.groceryitem, name = 'viewgroceryitem'),
    path('index', views.index, name = 'index')

]