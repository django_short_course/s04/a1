from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.

# THe from keyword allows importing of necessary classes/ modules, methods and others files needed in our application from the django.http package while the import keyword defines what we are importing from the package
from django.http import HttpResponse
# Local imports
from .models import ToDoItem, Event
# to use the template created:
from django.template import loader
# import the buil in user model
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
# import the authenticate function
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
from .forms import LoginForm, AddTaskForm, UpdateTaskForm, RegisterForm, AddEventForm, UpdateProfile
from django.utils import timezone
def index(request):
    todoitem_list = ToDoItem.objects.filter(user_id = request.user.id)
    event_list = Event.objects.filter(user_id = request.user.id)
    # template = loader.get_template("index.html")
    context = {
        'todoitem_list': todoitem_list,
        'event_list': event_list,
        'user': request.user
    }
    # output = ", ".join([todoitem.task_name for todoitem in todoitem_list])
    return render(request, "index.html", context)

def todoitem(request, todoitem_id):
    # response = f"You are viewing the details of {todoitem_id}"
    # return HttpResponse(response)
    todoitem = get_object_or_404(ToDoItem, pk = todoitem_id)

    return render(request, "todoitem.html", model_to_dict(todoitem))

def eventitem(request, eventitem_id):
    
    eventitem = get_object_or_404(Event, pk = eventitem_id)

    return render(request, "eventitem.html", model_to_dict(eventitem))

# this function is responsible for registering on our application

def register(request):
    context = {}    

    if request.method == 'POST':
        form = RegisterForm(request.POST)

        if form.is_valid() == False:
            form = RegisterForm()
        else:
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            confirmpassword = form.cleaned_data['confirmpassword']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']

            duplicate = User.objects.filter(username = username)      
            
            # is_match = False

            # if confirmpassword == password:
            #     return is_match == True
            # else:
            #     return is_match == False

            
            if not duplicate:
                User.objects.create(username = username, password = make_password(password), first_name = first_name, last_name = last_name, email = email)

                return redirect("todolist:login")
            
            else:
                context = {
                    
                    'error': True
                }

    return render(request, "register.html", context)

def change_password(request):
    
    is_user_authenticated = False

    user = authenticate(username = "johndoe", password = "john1234")

    if user is not None:
        authenticated_user = User.objects.get(username = 'johndoe')
        authenticated_user.set_password("johndoe12345")
        authenticated_user.save()

        is_user_authenticated = True
    
    context = {
        "is_user_authenticated" : is_user_authenticated
    }

    return render(request, "change_password.html", context)

def login_user(request):
    context = {}

    if request.method == "POST":
        form = LoginForm(request.POST)

        if form.is_valid() == False:
            form = LoginForm()
        else:
            username = form.cleaned_data["username"]
            password = form.cleaned_data['password']
            

            user = authenticate(username = username, password = password)

            if user is not None:
                context = {
                    'username' : username,
                    'password' : password
                }
                login(request, user)
                return redirect("todolist:index")
            else:
                context = {
                    'error' : True
                }
            
    return render(request, "login.html", context)


    # username = 'johndoe'
    # password = 'johndoe12345'

    # # is_user_authenticated = False
    
    # user = authenticate(username = username, password = password)
 
def logout_user(request):
    logout(request)
    return redirect("todolist:index")

def add_task(request):
    context = {}

    if request.method == 'POST':
        form = AddTaskForm(request.POST)

        if form.is_valid() == False:
            form = AddTaskForm()
        else:
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']

            duplicates = ToDoItem.objects.filter(task_name = task_name, user_id = request.user.id)

            if not duplicates:
                # create an object base on the Todoitem model and saves to the  record in the database
                ToDoItem.objects.create(task_name = task_name, description = description, date_created = timezone.now(), user_id = request.user.id)

                return redirect("todolist:index")
            
            else:
                context = {
                    'error': True
                }

    return render(request, "add_task.html", context)

def update_task(request, todoitem_id):

    todoitem = ToDoItem.objects.filter(pk = todoitem_id)

    context = {
        "user": request.user,
        "todoitem_id": todoitem_id,
        "task_name": todoitem[0].task_name,
        "description": todoitem[0].description,
        "status": todoitem[0].status
    }

    if request.method == "POST":
        form = UpdateTaskForm(request.POST)

        if form.is_valid() == False:
            form = UpdateTaskForm()
        
        else:
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']
            status = form.cleaned_data['status']

            if todoitem:

                todoitem[0].task_name = task_name
                todoitem[0].description = description
                todoitem[0].status = status

                todoitem[0].save()
                
                return redirect("todolist:viewtodoitem", todoitem_id = todoitem[0].id)
            
            else:

                context = {
                    "error": True
                }

    return render(request, "update_task.html", context)

def delete_task(request, todoitem_id):

    ToDoItem.objects.filter(pk = todoitem_id).delete()

    return redirect("todolist:index")

def add_event(request):
    context = {}

    if request.method == 'POST':
        form = AddEventForm(request.POST)
    
        if form.is_valid() == False:
            form = AddEventForm()
        else:
            event_name = form.cleaned_data['event_name']
            description = form.cleaned_data['description']

            duplicates = Event.objects.filter(event_name = event_name, user_id_id = request.user.id)

            if not duplicates:
                Event.objects.create(event_name = event_name, description = description, user_id_id = request.user.id)

                return redirect("todolist:index")
            
            else:
                context = {
                    'error': True
                }
    return render(request, "add_event.html", context)

def update_profile(request):
    
    user = User.objects.get(username = request.user.username)

    context = {
        'user': request.user,
        'first_name' : user.first_name,
        'last_name': user.last_name,
        'password': user.password
    }

    if request.method == "POST":
        form = UpdateProfile(request.POST)

        if form.is_valid() == False:
            form = UpdateProfile()
        else:
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            password = form.cleaned_data['password']

            if user:

                user.first_name = first_name
                user.last_name = last_name
                user.password = make_password(password)

                user.save()

                return redirect("todolist:index")
            
            else:
                context = {
                    'error' : True
                }
    
    return render(request, "update_profile.html", context)

